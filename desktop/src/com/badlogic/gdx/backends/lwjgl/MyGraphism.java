package com.badlogic.gdx.backends.lwjgl;

import com.kibo.game.Tools.PlayScreenWrapped;

import java.awt.Canvas;

/**
 * Created by willi on 19/03/2018.
 */

public class MyGraphism extends LwjglGraphics {


    MyGraphism(LwjglApplicationConfiguration config) {
        super(config);
    }

    MyGraphism(Canvas canvas) {
        super(canvas);
    }

    MyGraphism(Canvas canvas, LwjglApplicationConfiguration config) {
        super(canvas, config);
    }


    @Override
    void updateTime () {
        long time = System.nanoTime();
        deltaTime = PlayScreenWrapped.DELTATIME;//(time - lastTime) / 1000000000.0f;
        lastTime = time;

        if (time - frameStart >= 1000000000) {
            fps = frames;
            frames = 0;
            frameStart = time;
        }
        frames++;
    }
}
