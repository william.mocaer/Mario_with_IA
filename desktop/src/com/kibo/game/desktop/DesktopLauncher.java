package com.kibo.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl.MyApplication;
import com.kibo.game.MarioBrossTuto;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new MyApplication(new MarioBrossTuto(), config);
	}
}
