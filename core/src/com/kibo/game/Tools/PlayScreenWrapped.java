package com.kibo.game.Tools;

import com.badlogic.gdx.math.Vector2;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Mario;
import com.kibo.game.Tools.IA.GameRPZ;
import com.kibo.game.Tools.IA.StdOut;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by willi on 21/03/2018.
 */
public class PlayScreenWrapped implements GameRPZ {
    public static final int POSITION_ITERATIONS = 3;
    public static final int VELOCITY_ITERATIONS = 6;
    public static String[] ACTIONS = {"DROITE", "HAUT", "GAUCHE"};
    public static final float DELTATIME = 0.035f;//0.025

    private PlayScreen playScreen;
    private int profondeur;//la profondeur qui reste à chercher
    public String actionToRealize;

    public PlayScreenWrapped(int profondeur, MarioBrossTuto g) {
        this.profondeur = profondeur;
        playScreen = g.getSc();
      //  System.out.println("INIT FIRST");
    }

    public PlayScreenWrapped(int profondeur, String a, PlayScreen copie) {
        this.profondeur = profondeur;
        actionToRealize = a;
        this.playScreen = copie;
        realizeAction();
      //  System.out.println("INIT COPIE");

    }

    private void realizeAction() {
        playScreen.handleInput(DELTATIME,actionToRealize);//applique
        playScreen.getWorld().step(DELTATIME, VELOCITY_ITERATIONS, POSITION_ITERATIONS);//step
        playScreen.smallUpdate(DELTATIME);//update
        //playScreen.render(DELTATIME);
    }

    /*
    public List<PlayScreenWrapped> getEtatFinaux(int profondeurDeRecherche){
        if (profondeur==0)
        {
            LinkedList<PlayScreenWrapped> l  = new LinkedList<PlayScreenWrapped>();
            l.add(this);
            return l;
        }
        LinkedList<PlayScreenWrapped> l = new LinkedList<PlayScreenWrapped>();
        for ( PlayScreenWrapped w : this.getEtatSuivants())
        {
            l.addAll(w.getEtatFinaux(profondeurDeRecherche-1));
        }

        return l;
    }

    private List<GameRPZ> getEtatSuivants(){
        List<GameRPZ> l = new LinkedList<GameRPZ>();
        PlayScreen copie=CopierWorld.getCopie(this.getPlayScreen());
        for(int i=0;i<ACTIONS.length;++i){
            PlayScreenWrapped suivant = new PlayScreenWrapped(profondeur-1,ACTIONS[i],copie);
            l.add(suivant);
        }
        return l;
    }
    */

    @Override
    public boolean isGoal() {
        return profondeur == 0 || playScreen.IsMarioAtGoal();
    }

    @Override
    public com.kibo.game.Tools.IA.GameRPZ twin() {
        return null;
    }

    @Override
    public Iterable<com.kibo.game.Tools.IA.GameRPZ> neighbors() {
        List<com.kibo.game.Tools.IA.GameRPZ> l = new LinkedList<com.kibo.game.Tools.IA.GameRPZ>();
        if(!playScreen.getPlayer().isDead() && !(playScreen.getPlayer().b2body.getPosition().y<0.2) ) {
            for (int i = 0; i < ACTIONS.length; ++i) {
                PlayScreenWrapped suivant = new PlayScreenWrapped(profondeur - 1, ACTIONS[i], CopierWorld.getCopie(this.getPlayScreen()));
                l.add(suivant);
            }
        }

        return l;
    }

    @Override
    public int getDisorder() {
       /* System.out.println("play "+playScreen.getPlayer());
        System.out.println("body "+playScreen.getPlayer().b2body);*/
        int disoX = (int) (playScreen.getPlayer().b2body.getPosition().x * MarioBrossTuto.PPM - playScreen.getGoal().getX() * MarioBrossTuto.PPM);
        int disoY = (int) (playScreen.getPlayer().b2body.getPosition().y * MarioBrossTuto.PPM - playScreen.getGoal().getY() * MarioBrossTuto.PPM);
        int diso = (int)Math.sqrt(disoX*disoX+disoY*disoY);
        //diso = Math.abs(disoX);
        /*System.out.println("Y : "+playScreen.getPlayer().b2body.getPosition().y);*/
        if(disoX>=0)
            diso=0;
        if(actionToRealize!=null && actionToRealize.equals(ACTIONS[1]))
            diso+=1;
        if (playScreen.getPlayer().isDead() || playScreen.getPlayer().b2body.getPosition().y<0.2)
            diso += 99999;

        //System.out.println(actionToRealize);
        StdOut.println(diso);
        //StdOut.print(playScreen.getPlayer().getPosition());
        return diso;
    }

    @Override
    public boolean isSafe()
    {
        return playScreen.getPlayer().previousState== Mario.State.RUNNING && playScreen.getPlayer().currentState== Mario.State.RUNNING && playScreen.isMarioOnGround();
        /*playScreen.handleInput(DELTATIME,actionToRealize);//applique
        playScreen.getWorld().step(DELTATIME, VELOCITY_ITERATIONS, POSITION_ITERATIONS);//step
        playScreen.smallUpdate(DELTATIME);*/
        //return
    }

    public PlayScreen getPlayScreen() {
        return playScreen;
    }

    public void setPlayScreen(PlayScreen playScreen) {
        this.playScreen = playScreen;
    }

    public String getActionToRealize() {
        return actionToRealize;
    }

    public String toString() {
        return actionToRealize;
    }

    public float getReelAvancement(GameRPZ autreGame){
        Vector2 vec = ((PlayScreenWrapped) autreGame).getPlayScreen().getPlayer().b2body.getPosition().sub(getPlayScreen().getPlayer().b2body.getPosition()).scl(MarioBrossTuto.PPM);
        float dxcar =vec.x*vec.x;
        float dycar =vec.y*vec.y;
        return (float)Math.sqrt(dxcar+dycar);
    }
}
