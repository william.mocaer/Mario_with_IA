package com.kibo.game.Tools;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Sprites.Enemies.Enemy;
import com.kibo.game.Sprites.Items.Item;
import com.kibo.game.Sprites.Mario;
import com.kibo.game.Sprites.TilesObjects.InteractiveTileObject;

/**
 * Created by willi on 19/05/2017.
 */

public class WorldContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();

        int cDef = fixA.getFilterData().categoryBits | fixB.getFilterData().categoryBits;

       // System.out.println("\nCONTACT");


        switch (cDef)
        {
            case MarioBrossTuto.MARIO_HEAD_BIT | MarioBrossTuto.BRICK_BIT:
            case MarioBrossTuto.MARIO_HEAD_BIT | MarioBrossTuto.COIN_BIT:
                if (fixA.getFilterData().categoryBits == MarioBrossTuto.MARIO_HEAD_BIT)
                    ((InteractiveTileObject)fixB.getUserData()).onHeadHit((Mario)fixA.getUserData());
                else
                    ((InteractiveTileObject)fixA.getUserData()).onHeadHit((Mario)fixB.getUserData());

                break;
            case MarioBrossTuto.ENEMY_HEAD_BIT | MarioBrossTuto.MARIO_BIT:
                if (fixA.getFilterData().categoryBits == MarioBrossTuto.ENEMY_HEAD_BIT)
                    ((Enemy)fixA.getUserData()).hitOnHead((Mario)fixB.getUserData());
                else
                    ((Enemy)fixB.getUserData()).hitOnHead((Mario)fixA.getUserData());
                break;
            case MarioBrossTuto.ENEMY_HEAD_BIT | MarioBrossTuto.OBJECT_BIT:
            case MarioBrossTuto.ENEMY_BIT | MarioBrossTuto.OBJECT_BIT:
                if (fixA.getFilterData().categoryBits == MarioBrossTuto.OBJECT_BIT)
                    ((Enemy)fixB.getUserData()).reverseVelocity(true,false);
                else
                    ((Enemy)fixA.getUserData()).reverseVelocity(true,false);
                break;
            case MarioBrossTuto.ENEMY_HEAD_BIT:
            case MarioBrossTuto.ENEMY_BIT:
                ((Enemy)fixA.getUserData()).onEnemyHit((Enemy)fixB.getUserData());
                ((Enemy)fixB.getUserData()).onEnemyHit((Enemy)fixA.getUserData());

                break;
            case MarioBrossTuto.MARIO_BIT | MarioBrossTuto.ENEMY_BIT:
                if (fixA.getFilterData().categoryBits == MarioBrossTuto.MARIO_BIT)
                    ((Mario)fixA.getUserData()).hit((Enemy)fixB.getUserData());
                else
                    ((Mario)fixB.getUserData()).hit((Enemy)fixA.getUserData());
               // System.out.println("\nMARIO ENEMY");

                break;

            case MarioBrossTuto.ITEM_BIT | MarioBrossTuto.OBJECT_BIT:
                if (fixA.getFilterData().categoryBits == MarioBrossTuto.ITEM_BIT)
                    ((Item)fixA.getUserData()).reverseVelocity(true,false);
                else
                    ((Item)fixB.getUserData()).reverseVelocity(true,false);
                break;
            case MarioBrossTuto.ITEM_BIT | MarioBrossTuto.MARIO_BIT :
                if (fixA.getFilterData().categoryBits == MarioBrossTuto.ITEM_BIT)
                {
                    if (! (fixB.getUserData() instanceof Mario))
                        break;
                    ((Item)fixA.getUserData()).use((Mario)fixB.getUserData());
                }
                else
                {
                    if (! (fixA.getUserData() instanceof Mario))
                        break;
                    ((Item)fixB.getUserData()).use((Mario)fixA.getUserData());
                }
                break;
        }

    }


    @Override
    public void endContact(Contact contact)
    {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
