package com.kibo.game.Tools;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Enemies.Goomba;
import com.kibo.game.Sprites.Enemies.Turtle;
import com.kibo.game.Sprites.Items.ItemDef;
import com.kibo.game.Sprites.Items.Mushroom;
import com.kibo.game.Sprites.Mario;
import com.kibo.game.Sprites.TilesObjects.Brick;
import com.kibo.game.Sprites.TilesObjects.Coin;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by willi on 19/03/2018.
 */

public class CopierWorld {



    public static PlayScreen getCopie(PlayScreen playScreen) {

        PlayScreen pl = new PlayScreen();
        BodiesWrapped bodiesWrappedAndWorld = new BodiesWrapped();
        pl.setAtlas(playScreen.getAtlas());
        pl.setMap(playScreen.getMap());
        getCopie(playScreen.getWorld(),pl,bodiesWrappedAndWorld);//Mario ; world
        B2WorldCreator creator = new B2WorldCreator();
        creator.setGoombas(bodiesWrappedAndWorld.goombas);
        creator.setTurtles(bodiesWrappedAndWorld.turtles);
        creator.grounds=playScreen.getCreator().grounds;

        bodiesWrappedAndWorld.game = playScreen.getGame();
        bodiesWrappedAndWorld.map = playScreen.getMap();

        bodiesWrappedAndWorld.creator = creator;
        bodiesWrappedAndWorld.creator.goal=playScreen.getCreator().getGoal();
        for(Goomba g : bodiesWrappedAndWorld.goombas)
            g.setCreator(creator);
        for(Turtle t : bodiesWrappedAndWorld.turtles)
            t.setCreator(creator);
        bodiesWrappedAndWorld.atlas=playScreen.getAtlas();
        bodiesWrappedAndWorld.worldContactListener=playScreen.getWorldContactListener();
        bodiesWrappedAndWorld.world.setContactListener(bodiesWrappedAndWorld.worldContactListener);
        bodiesWrappedAndWorld.itemsToSpawn=new LinkedBlockingQueue<ItemDef>(playScreen.getItemsToSpawn());

        pl.hydrate(bodiesWrappedAndWorld);
        //constructor to hydrate
        /*
        * hud ?
        * items && itemToSpawn
        * */
        return pl;
    }

    public static void getCopie(World w, PlayScreen pl, BodiesWrapped bodiesWrapped)
    {
        World world=new World(w.getGravity(),true);
        Array<Body> b =new Array<Body>();
        w.getBodies(b);
        for (Body body : b)
        {
            BodyDef bdef = new BodyDef();
            Vector2 pos = body.getPosition();
            bdef.position.set(pos.x,pos.y);
            bdef.type = body.getType();
            bdef.linearVelocity.set(body.getLinearVelocity());
            bdef.angularVelocity = body.getAngularVelocity();
            Body body1 = world.createBody(bdef);
            boolean added = false;
            Object newUserData = null;
            for (Fixture f :body.getFixtureList())
            {
                FixtureDef fdef = new FixtureDef();
                fdef.shape=f.getShape();
                fdef.filter.categoryBits= f.getFilterData().categoryBits;
                fdef.filter.maskBits=f.getFilterData().maskBits;
                fdef.restitution=f.getRestitution();
                Object userData = f.getUserData();
                if(!added)
                {
                    if(userData instanceof Mario)
                    {
                        Mario m = new Mario(pl,body1);
                        m.currentState=((Mario)userData).currentState;
                        m.previousState=((Mario)userData).previousState;
                       // body1.setLinearVelocity(((Mario)userData).b2body.getLinearVelocity());
                        bodiesWrapped.mario=m;
                        added=true;
                        body1.createFixture(fdef).setUserData(m);
                        newUserData =m;

                    }else if(userData instanceof Turtle)
                    {
                        Turtle t =new Turtle(pl,body1);
                        t.velocity = (((Turtle)userData).velocity);
                        /*body1.setLinearVelocity(((Turtle)userData).velocity);*/
                        body1.setActive((((Turtle)userData).b2body.isActive()));
                        bodiesWrapped.turtles.add(t);
                        added=true;
                        body1.createFixture(fdef).setUserData(t);
                        newUserData =t;


                    }else if(userData instanceof Goomba)
                    {
                        Goomba g = new Goomba(pl,body1);
                        g.velocity = (((Goomba)userData).velocity);
                        /*body1.setLinearVelocity( g.velocity);*/
                        body1.setActive((((Goomba)userData).b2body.isActive()));

                        bodiesWrapped.goombas.add(g);
                        added=true;
                        body1.createFixture(fdef).setUserData(g);
                        newUserData =g;


                    }else if(userData instanceof Mushroom)
                    {
                        Mushroom m = new Mushroom(pl,body1);
                        bodiesWrapped.items.add(m);
                        added=true;
                        body1.createFixture(fdef).setUserData(m);
                        newUserData =m;

                    }else if(userData instanceof Brick)
                    {
                        Brick brick =((Brick)userData);

                        Brick br = new Brick(pl,body1);
                        bodiesWrapped.interactiveTileObjects.add(br);
                        added=true;
                        body1.createFixture(fdef).setUserData(br);
                        newUserData =br;

                    }else if(userData instanceof Coin)
                    {
                        Coin c = new Coin(pl,body1);
                        bodiesWrapped.interactiveTileObjects.add(c);
                        added=true;
                        body1.createFixture(fdef).setUserData(c);
                        newUserData =c;

                    }else
                    {
                        body1.createFixture(fdef).setUserData(userData);
                        newUserData=userData;
                    }

                }else
                    body1.createFixture(fdef).setUserData(newUserData);

            }

        }

        bodiesWrapped.world = world;
    }







}
