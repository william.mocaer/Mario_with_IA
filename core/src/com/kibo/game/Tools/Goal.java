package com.kibo.game.Tools;

/**
 * Created by willi on 03/04/2018.
 */

public class Goal {
    private final float x;
    private final float y;

    public Goal(float x,float y) {
        this.x=x;
        this.y=y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
