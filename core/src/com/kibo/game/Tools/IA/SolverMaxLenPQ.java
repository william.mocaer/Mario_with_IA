package com.kibo.game.Tools.IA;

import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Tools.PlayScreenWrapped;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SolverMaxLenPQ {

    private final GameRPZ initialGame;
    private SearchNode solution=null;
    private List<GameRPZ> pathSolution;

    int maxSize = 1250;

    private static class SearchNode implements Comparable<SearchNode> {

        private SearchNode preced;
        private int depth;
        private GameRPZ game;
        private int disorder;
        float distanceParcouru;


        public SearchNode(SearchNode p,GameRPZ b,int depth,float distanceParcouru) {
            preced=p;
            this.game=b;
            this.depth=depth;
            disorder=b.getDisorder();
            this.distanceParcouru=distanceParcouru;
        }

        public SearchNode(GameRPZ b,int depth,int disorder) {
            preced=null;
            this.game=b;
            this.depth=depth;
            this.disorder=disorder;
            this.distanceParcouru=0;
        }


        private int getWeight(){
            return disorder+depth;
        }

        @Override
        public int compareTo(SearchNode searchNode) {
            return getWeight()-searchNode.getWeight();
        }

        public boolean isGoal(){
            return game.isGoal();
        }

        public boolean isSafe() {
            return game.isSafe();
        }
    }

    public SolverMaxLenPQ(GameRPZ initial) {
        initialGame=initial;
        pathSolution = new LinkedList<GameRPZ>();
        aStar();
    }

    private void aStar() {

        MinPQ<SearchNode> priorityQueue = new MinPQ<SearchNode>();

        priorityQueue.insert(new SearchNode(null, initialGame,0,0));

        SearchNode current=priorityQueue.delMin();

        int realDepth = 0;

        while(!current.isGoal()){



            Iterable<GameRPZ> neibords = current.game.neighbors();
            for(GameRPZ g:neibords){
                if(current.preced==null || !g.equals(current.preced.game)){
                    priorityQueue.insert(new SearchNode(current,g,current.depth+1,current.game.getReelAvancement(g)));
                    realDepth = Math.max(realDepth,current.depth+1);
                }
            }
            //StdOut.println("size "+priorityQueue.size());
            if(priorityQueue.size()> maxSize){

                StdOut.println("Nouveau départ !");
                //SearchNode safeNode = getSafeNode(priorityQueue);
                // updateSolution(safeNode);//se charge de modifier l'attr pathSolution, pour sauvegarder la solution jusqu'au safeNode
                MinPQ<SearchNode> newpq = new MinPQ<SearchNode>();
                for(int i=0;i<600;i++)
                    newpq.insert(priorityQueue.delMin());
                priorityQueue = newpq;//on dit que le safeNode devient le nouveau départ
                //priorityQueue.insert(new SearchNode(null, safeNode.game, 0));//pas sûr que ça soit utile
                realDepth=0;//on fait comme si on reprenait la recherche à zéro
            }
            current=priorityQueue.delMin();
        }

        if(current.game.isGoal())
            updateSolution(current);

    }

    private void updateSolution(SearchNode safeNode) {

     /*   if(pathSolution.size()==0)
        int index = pathSolution.indexOf(safeNode.game);//utilise la reference
        if(index==-1)
            throw new RuntimeException("Erreur lors de l'update");
        for (int i = pathSolution.size()-1; i >index ; i--)
        {
            pathSolution.remove(i);
        }*/
        LinkedList<GameRPZ> solTmp= new LinkedList<GameRPZ>();
        while (safeNode.preced!=null){

            solTmp.add(safeNode.game);
            safeNode=safeNode.preced;
        }
        Collections.reverse(solTmp);
        pathSolution.addAll(solTmp);


    }

    private SearchNode getSafeNode(MinPQ<SearchNode> liste){
        SearchNode node = null;
        do{
            node = liste.delMin();
            if(node.isSafe())
            {
                return node;
            }
        }while(liste.isEmpty());
        return node;
    }

    public int moves() {
        return pathSolution.size();
    }

    public Iterable<GameRPZ> solution() {
        //Collections.reverse(pathSolution);

        return pathSolution;
    }

    public static List<String> solveMain(MarioBrossTuto gameM) {
        // create initial board from file
       /* In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();*/

        System.out.println("Initialisation du bord");

        GameRPZ initial = new PlayScreenWrapped(15000, gameM);

        System.out.println("Jeu à résoudre : \n"+initial);

        // solve the puzzle
        System.out.println("Début de la résolution du jeu...");
        SolverMaxLenPQ solver = new SolverMaxLenPQ(initial);
        System.out.println("Le jeu est résolu\n");

        // print solution to standard output
       /* if (!solver.isSolvable())
            StdOut.println("No solution possible");*/
        List<String> solution = new LinkedList<String>();

        StdOut.println("Minimum number of moves = " + solver.moves());
        for (GameRPZ game : solver.solution())
        {
            solution.add(game.toString());

        }
        return solution;
    }
}
