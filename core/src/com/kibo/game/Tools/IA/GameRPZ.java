package com.kibo.game.Tools.IA;

public interface GameRPZ {

    boolean isGoal();

    GameRPZ twin();

    boolean equals(Object that);

    Iterable<GameRPZ> neighbors();

    String toString();

    int getDisorder();

    boolean isSafe();

    float getReelAvancement(GameRPZ autreGame);
}