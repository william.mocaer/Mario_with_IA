package com.kibo.game.Tools.IA;

import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Tools.PlayScreenWrapped;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Solver {

    private final GameRPZ initialGame;
    private SearchNode solution=null;
    private boolean solvable = false;

    private static class SearchNode implements Comparable<SearchNode> {

        private SearchNode preced;
        private int depth;
        private GameRPZ game;
        private int disorder;


        public SearchNode(SearchNode p,GameRPZ b,int depth) {
            preced=p;
            this.game=b;
            this.depth=depth;
            disorder=b.getDisorder();
        }

        private int getWeight(){
            return depth+disorder;
        }

        @Override
        public int compareTo(SearchNode searchNode) {
            return getWeight()-searchNode.getWeight();
        }

        public boolean isGoal(){
            return game.isGoal();
        }
    }

    public Solver(GameRPZ initial) {
        initialGame=initial;
        aStar();
    }

    private void aStar() {
        MinPQ<SearchNode> priorityQueue1 = new MinPQ<SearchNode>();

        priorityQueue1.insert(new SearchNode(null, initialGame,0));

        SearchNode current1=priorityQueue1.delMin();

        while(!current1.isGoal()){

            Iterable<GameRPZ> neibords1 = current1.game.neighbors();

            for(GameRPZ g:neibords1){
                if(current1.preced==null || !g.equals(current1.preced.game)){
                    priorityQueue1.insert(new SearchNode(current1,g,current1.depth+1));
                }
            }

            current1=priorityQueue1.delMin();
        }

        if(current1.game.isGoal()){
            solution=current1;
            solvable=true;
        }else{
            solvable=false;
        }

    }

    public boolean isSolvable() {
        return solvable;
    }

    public int moves() {
        return solution.depth;
    }

    public Iterable<GameRPZ> solution() {
        LinkedList<GameRPZ> solu = new LinkedList<GameRPZ>();

        while (solution.preced!=null){
            solu.add(solution.game);
            solution=solution.preced;
        }

        solu.add(solution.game);

        Collections.reverse(solu);

        return solu;
    }

    public static List<String> solveMain(MarioBrossTuto gameM) {

        System.out.println("Initialisation du bord");
        GameRPZ initial = new PlayScreenWrapped(10000, gameM);

        System.out.println("Jeu à résoudre : \n"+initial);

        // solve the puzzle
        System.out.println("Début de la résolution du jeu...");
        Solver solver = new Solver(initial);
        System.out.println("Le jeu est résolu\n");

        // print solution to standard output

        List<String> solution = new LinkedList<String>();

        if (!solver.isSolvable())
            StdOut.println("No solution possible");

        StdOut.println("Minimum number of moves = " + solver.moves());
        for (GameRPZ game : solver.solution()){
            //StdOut.print(game+", ");
           if(game!=null && game.toString()!=null && game.toString().equals(PlayScreenWrapped.ACTIONS[1]))
            {
                StdOut.println("JUMP BEFORE "+((PlayScreenWrapped)game).getPlayScreen().getPlayer().b2body.getPosition().x+"" +
                        " ;; "+((PlayScreenWrapped)game).getPlayScreen().getPlayer().b2body.getPosition().y+" " +
                        "states :cur "+((PlayScreenWrapped)game).getPlayScreen().getPlayer().currentState+"  -old;" +
                        ((PlayScreenWrapped)game).getPlayScreen().getPlayer().previousState);

            }
            /*Goomba enemy = ((PlayScreenWrapped) game).getPlayScreen().getGoomba();
            System.out.println("GOOMBA POSITION : " +enemy.b2body.getPosition().x+" velo ("+enemy.b2body.getLinearVelocity().x+"; "+enemy.b2body.getLinearVelocity().y+" ACTIV ? "+enemy.b2body.isActive());
            */
           // System.out.println("\n VELOCITY "+((PlayScreenWrapped)game).getPlayScreen().getPlayer().b2body.getLinearVelocity()+" ;; ");
            solution.add(game.toString());
        }

        return solution;
    }
}
