package com.kibo.game.Tools.IA;

import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Tools.PlayScreenWrapped;

import java.util.LinkedList;
import java.util.List;

public class SolverIDASTART_Wiki {

    //cette version implémente l'algo de IDA* donne à l'adresse suivante
    //https://en.wikipedia.org/wiki/Iterative_deepening_A*

    private final GameRPZ initialGame;
    List<GameRPZ> solution = null;

    public SolverIDASTART_Wiki(GameRPZ initial) {
        initialGame=initial;
        solution = IdAStar();
    }

    private List<GameRPZ> IdAStar() {
        int bound = initialGame.getDisorder();
        LinkedList<GameRPZ> path = new LinkedList<GameRPZ>();
        path.add(initialGame);
        while(true){
            int t = search(path,0,bound);
            if(t==0) return path;
            bound = t;
        }
    }

    private int search(List<GameRPZ> path, int g, int bound){
        GameRPZ node = path.get(path.size()-1);
        int f = g + node.getDisorder();
        if(f>bound) return f;
        if (node.isGoal()) return 0;
        int min = Integer.MAX_VALUE;
        for(GameRPZ succ : node.neighbors()){
            if(!path.contains(succ)){
                path.add(succ);
                int t = search(path,g+1,bound);
                if(t==0) return 0;
                if(t<min) min=t;
                path.remove(path.size()-1);
            }
        }
        return min;
    }

    public static List<String> solveMain(MarioBrossTuto gameM) {
        // create initial board from file

        System.out.println("Initialisation du bord");

        GameRPZ initial =  new PlayScreenWrapped(10000, gameM);

        System.out.println("Jeu à résoudre : \n"+initial);

        // solve the puzzle
        System.out.println("Début de la résolution du jeu...");
        SolverIDASTART_Wiki solver = new SolverIDASTART_Wiki(initial);
        System.out.println("Le jeu est résolu\n");

        // print solution to standard output
        /*
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        */
        List<String> solution = new LinkedList<String>();
        for (GameRPZ game : solver.solution)
            solution.add(game.toString());

        return solution;

    }
}
