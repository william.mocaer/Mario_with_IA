package com.kibo.game.Tools;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.World;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Sprites.Enemies.Goomba;
import com.kibo.game.Sprites.Enemies.Turtle;
import com.kibo.game.Sprites.Items.Item;
import com.kibo.game.Sprites.Items.ItemDef;
import com.kibo.game.Sprites.Mario;
import com.kibo.game.Sprites.TilesObjects.InteractiveTileObject;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by willi on 24/03/2018.
 */

public class BodiesWrapped {
    public MarioBrossTuto game;
    public LinkedList<Turtle> turtles;
    public LinkedList<Goomba> goombas;
    public Mario mario;
    public List<Item> items;
    public List<InteractiveTileObject> interactiveTileObjects;
    public World world;
    public B2WorldCreator creator;
    public TextureAtlas atlas;
    public WorldContactListener worldContactListener;
    public TiledMap map;
    public LinkedBlockingQueue<ItemDef> itemsToSpawn;
    public Goal goal;
    public List<Rectangle> grounds;

    public BodiesWrapped() {
        goombas=new LinkedList<Goomba>();
        turtles = new LinkedList<Turtle>();
        items=new LinkedList<Item>();
        interactiveTileObjects= new LinkedList<InteractiveTileObject>();
    }
}
