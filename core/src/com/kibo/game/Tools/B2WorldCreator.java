package com.kibo.game.Tools;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Enemies.Enemy;
import com.kibo.game.Sprites.Enemies.Goomba;
import com.kibo.game.Sprites.Enemies.Turtle;
import com.kibo.game.Sprites.TilesObjects.Brick;
import com.kibo.game.Sprites.TilesObjects.Coin;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by willi on 19/05/2017.
 */

public class B2WorldCreator {

    public static final int LAYER_GRAPHIC=1;
    public static final int LAYER_GROUND=2;
    public static final int LAYER_PIPE=3;
    public static final int LAYER_COIN=4;
    public static final int LAYER_BRICS=5;
    public static final int LAYER_GOOMBA=6;
    public static final int LAYER_TURTLE=7;
    private static final int LAYER_GOAL = 8;
    public List<Rectangle> grounds;
    public  Goal goal;


    private List<Goomba> goombas;



    private List<Turtle> turtles;


    public B2WorldCreator()
    {
    }
    public B2WorldCreator(PlayScreen screen)
    {
        World world = screen.getWorld();
        TiledMap map = screen.getMap();

        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;
        grounds = new LinkedList<Rectangle>();
        //create gound bodies/fixtures
        for(MapObject object : map.getLayers().get(LAYER_GROUND).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject)object).getRectangle();
            grounds.add(rect);
            bdef.type= BodyDef.BodyType.StaticBody;
            bdef.position.set( (rect.getX() + rect.getWidth()/2)/ MarioBrossTuto.PPM,( rect.getY() + rect.getHeight()/2)/ MarioBrossTuto.PPM);

            body = world.createBody(bdef);

            shape.setAsBox(rect.getWidth()/2/ MarioBrossTuto.PPM, rect.getHeight()/2/ MarioBrossTuto.PPM);
            fdef.shape=shape;
            body.createFixture(fdef);

        }

        //create pipe bodies/fixtures
        for(MapObject object : map.getLayers().get(LAYER_PIPE).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject)object).getRectangle();
            bdef.type= BodyDef.BodyType.StaticBody;
            bdef.position.set( (rect.getX() + rect.getWidth()/2)/ MarioBrossTuto.PPM , (rect.getY() + rect.getHeight()/2)/MarioBrossTuto.PPM);

            body = world.createBody(bdef);

            shape.setAsBox(rect.getWidth()/2/ MarioBrossTuto.PPM, rect.getHeight()/2/ MarioBrossTuto.PPM);
            fdef.shape=shape;
            fdef.filter.categoryBits = MarioBrossTuto.OBJECT_BIT;
            body.createFixture(fdef);

        }

        //create bricks bodies/fixtures
        for(MapObject object : map.getLayers().get(LAYER_BRICS).getObjects().getByType(RectangleMapObject.class))
        {
            new Brick(screen,object);
        }

        //create coin bodies/fixtures
        for(MapObject object : map.getLayers().get(LAYER_COIN).getObjects().getByType(RectangleMapObject.class))
        {
            new Coin(screen,object);
        }

        //create all goombas
        goombas = new LinkedList<Goomba>();
        for(MapObject object : map.getLayers().get(LAYER_GOOMBA).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject)object).getRectangle();
            goombas.add(new Goomba(screen,rect.getX() / MarioBrossTuto.PPM,(rect.getY()+5)/ MarioBrossTuto.PPM));
        }

        turtles = new LinkedList<Turtle>();
        for(MapObject object : map.getLayers().get(LAYER_TURTLE).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject)object).getRectangle();
            turtles.add(new Turtle(screen,rect.getX() / MarioBrossTuto.PPM,(rect.getY()+5)/ MarioBrossTuto.PPM));
        }

        for(MapObject object : map.getLayers().get(LAYER_GOAL).getObjects().getByType(RectangleMapObject.class))
        {
            Rectangle rect = ((RectangleMapObject)object).getRectangle();
            goal = new Goal(rect.getX()/MarioBrossTuto.PPM,rect.getY()/MarioBrossTuto.PPM);
        }
    }

    public List<Goomba> getGoombas() {
        return goombas;
    }

    public void destroyTurtle(Turtle turtle)
    {
        turtles.remove(turtle);
    }
    public List<Enemy> getEnemies() {
        List<Enemy> enemies = new LinkedList<Enemy>();
        enemies.addAll(goombas);
        enemies.addAll(turtles);
        return enemies;
    }
    public void setTurtles(List<Turtle> turtles) {
        this.turtles = turtles;
    }

    public void setGoombas(List<Goomba> goombas) {
        this.goombas = goombas;
    }

    public Goal getGoal() {
        return goal;
    }
}
