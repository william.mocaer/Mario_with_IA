package com.kibo.game.Tools;

/**
 * Created by willi on 08/04/2018.
 */

public class Test {
    public enum State {FALLING, JUMPING, STANDING, RUNNING, GROWING, DEAD};

    public static void main(String[] args)
    {
        State current;


        current = State.FALLING;

        System.out.println("current = "+current);

        State s3 = State.DEAD;
        current=s3;
        current=State.RUNNING;

        System.out.println("s3 = "+s3);
        System.out.println("current = "+current);

    }
}
