package com.kibo.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.StringBuilder;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Screens.WelcomeScreen;
import com.kibo.game.Tools.PlayScreenWrapped;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class MarioBrossTuto extends Game {

	public static String map= "level5.tmx";
	private boolean load=false;

	/*public static final int V_WIDTH=400;
	public static final int V_HEIGHT=208;*/
	public static final int V_WIDTH=(int)(800);
	public static final int V_HEIGHT=(int)(480);
	public static final float PPM = 100;

    public static final short GROUND_BIT =1;
	public static final short MARIO_BIT=2;
	public static final short BRICK_BIT=4;
	public static final short COIN_BIT=8;
	public static final short DESTROYED_BIT=16;
    public static final short OBJECT_BIT = 32;
    public static final short ENEMY_BIT = 64;
    public static final short ENEMY_HEAD_BIT = 128;
	public static final short ITEM_BIT = 256;
	public static final short MARIO_HEAD_BIT = 512;

    public static SpriteBatch batch;

	public static AssetManager manager;
    public PlayScreen sc;
	public List<String> solution;
	private float accumulator;
	private Skin skin;

	public MarioBrossTuto()
	{
		super();
		accumulator=0;

	}
	@Override
	public void create () {
		batch = new SpriteBatch();
        manager=new AssetManager();
        manager.load("audio/music/mario_music.ogg",Music.class);
        manager.load("audio/sounds/coin.wav",Sound.class);
        manager.load("audio/sounds/bump.wav",Sound.class);
        manager.load("audio/sounds/breakblock.wav",Sound.class);
		manager.load("audio/sounds/powerup_spawn.wav",Sound.class);
		manager.load("audio/sounds/powerup.wav",Sound.class);
		manager.load("audio/sounds/powerdown.wav",Sound.class);
		manager.load("audio/sounds/stomp.wav",Sound.class);
		manager.load("audio/sounds/mariodie.wav",Sound.class);

		manager.finishLoading();
		skin= new Skin(Gdx.files.internal( "skin/star-soldier/skin/star-soldier-ui.json" ));

		/*sc = new PlayScreen(this);
		solution=new LinkedList<String>();
		if(!load)
		{
			//solution = SolverJump.solveMain(this);
			solution = SolverMaxLenPQ.solveMain(this);
			//solution = Solver.solveMain(this);
			//solution = SolverIDASTART_Wiki.solveMain(this);
			//solution.remove(0);
		}else {
			try {
				loadSolution();
			} catch (GdxRuntimeException e) {
				System.out.println("SOLUTION N'existe pas, calcul");
			}
		}

		sc.setActions(solution);*/
		setScreen(new WelcomeScreen(this));
	}

	public void exportSolution()
	{
		FileHandle fichier = Gdx.files.local("solutions/"+map+".sol");
		StringBuilder sb = new StringBuilder();
		for (String str : solution)
			sb.append(str).append(" ");

		fichier.writeString(sb.toString(), false);
	}

	public void loadSolution()
	{

		FileHandle fichier = Gdx.files.local("solutions/"+map+".sol");
		String[] texte = fichier.readString().split(" ");
		solution=new LinkedList<String>();
		Collections.addAll(solution, texte);

	}

	@Override
	public void render ()
    {
		float dt = Gdx.graphics.getDeltaTime();
		accumulator += dt;
		if (screen != null)
		{
			if(screen instanceof PlayScreen && accumulator>= PlayScreenWrapped.DELTATIME && ((PlayScreen)screen).itAction!=null)
			{
				screen.render(PlayScreenWrapped.DELTATIME);
				accumulator=0;
			}else
				screen.render(dt);

		}

		//manager.update();
    }
	
	@Override
	public void dispose () {
		super.dispose();
        manager.dispose();
        batch.dispose();

	}

    public PlayScreen getSc() {
        return sc;
    }

	public Skin getSkin() {
		return skin;
	}
}
