package com.kibo.game.Sprites.Items;

import com.badlogic.gdx.math.Vector2;

/**
 * Created by willi on 21/05/2017.
 */

public class ItemDef
{
    public Vector2 position;
    public Class<?> type;
    public ItemDef(Vector2 position,Class<?> type)
    {
        this.position=position;
        this.type=type;
    }
}
