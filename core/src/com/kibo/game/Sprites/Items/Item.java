package com.kibo.game.Sprites.Items;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Mario;

/**
 * Created by willi on 21/05/2017.
 */

public abstract class Item extends Sprite{
    protected PlayScreen screen;
    protected Vector2 velocity;
    protected boolean toDestroy;
    protected boolean destroyed;
    protected Body body;

    public Item(PlayScreen screen, float x, float y)
    {
        this.screen=screen;
        setPosition(x,y);
        setBounds(getX(),getY(), 16/ MarioBrossTuto.PPM,16/MarioBrossTuto.PPM);
        defineItem();
        toDestroy=false;
        destroyed=false;
    }

    public Item(PlayScreen screen,Body body)
    {
        this.screen=screen;
        setBounds(getX(),getY(), 16/ MarioBrossTuto.PPM,16/MarioBrossTuto.PPM);

    }

    public abstract void defineItem();
    public abstract void use(Mario mario);

    public void update(float dt)
    {
        if(toDestroy && !destroyed)
        {
            screen.getWorld().destroyBody(body);
            destroyed=true;
        }
    }


    public void draw(Batch batch)
    {
        if(!destroyed)
            super.draw(batch);
    }

    public void destroy()
    {
        toDestroy=true;
    }

    public void reverseVelocity(boolean x,boolean y)
    {
        if(x)
            velocity.x=-velocity.x;
        if(y)
            velocity.y=-velocity.y;
    }
}
