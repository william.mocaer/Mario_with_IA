package com.kibo.game.Sprites.Items;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Mario;

/**
 * Created by willi on 21/05/2017.
 */

public class Mushroom extends Item {
    public Mushroom(PlayScreen screen, float x, float y) {
        super(screen, x, y);
        setRegion(screen.getAtlas().findRegion("mushroom"),0,0,16,16);
        velocity = new Vector2(0.7f,0);
    }

    public Mushroom(PlayScreen screen, Body body) {
        super(screen,body);
        setRegion(screen.getAtlas().findRegion("mushroom"),0,0,16,16);
        velocity = new Vector2(0.7f,0);
    }

    @Override
    public void defineItem() {
        BodyDef bdef=new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        body =  screen.getWorld().createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6/ MarioBrossTuto.PPM);
        fdef.filter.categoryBits = MarioBrossTuto.ITEM_BIT;
        fdef.filter.maskBits= MarioBrossTuto.MARIO_BIT |
                MarioBrossTuto.OBJECT_BIT |
                MarioBrossTuto.COIN_BIT |
                MarioBrossTuto.GROUND_BIT |
                MarioBrossTuto.BRICK_BIT;

        fdef.shape=shape;
        body.createFixture(fdef).setUserData(this );
    }

    @Override
    public void use(Mario mario) {
        destroy();
        if(!mario.isBig())
            mario.grow();
    }

    public void update(float dt)
    {
        super.update(dt);
        setPosition(body.getPosition().x - getWidth()/2, body.getPosition().y - getHeight()/2);
        velocity.y=body.getLinearVelocity().y;
        body.setLinearVelocity(velocity);
    }

}
