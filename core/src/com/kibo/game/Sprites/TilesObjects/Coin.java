package com.kibo.game.Sprites.TilesObjects;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Scenes.Hud;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Items.ItemDef;
import com.kibo.game.Sprites.Items.Mushroom;
import com.kibo.game.Sprites.Mario;

/**
 * Created by willi on 19/05/2017.
 */

public class Coin extends InteractiveTileObject {
    private static TiledMapTileSet tileSet;
    private final int BLANK_COIN = 28;

    public Coin(PlayScreen screen, MapObject object)
    {
        super(screen, object);
        tileSet= map.getTileSets().getTileSet("tileset_gutter");
        fixture.setUserData(this);
        setCategoryFilter(MarioBrossTuto.COIN_BIT);
    }

    public Coin(PlayScreen screen, Body body1)
    {
        super(screen, body1);
        tileSet= map.getTileSets().getTileSet( "tileset_gutter");
        //fixture.setUserData(this);
        setCategoryFilter(MarioBrossTuto.COIN_BIT);
    }

    @Override
    public void onHeadHit(Mario mario) {

        if(getCell().getTile().getId() == BLANK_COIN)
        {
            MarioBrossTuto.manager.get("audio/sounds/bump.wav", Sound.class).play();
        }else
        {
            if(object.getProperties().containsKey("mushroom"))
            {
                screen.spawnItem(new ItemDef(new Vector2(body.getPosition().x,body.getPosition().y+16 / MarioBrossTuto.PPM), Mushroom.class));
                MarioBrossTuto.manager.get("audio/sounds/powerup_spawn.wav",Sound.class).play();
            }else
            {
                MarioBrossTuto.manager.get("audio/sounds/coin.wav", Sound.class).play();

            }
            getCell().setTile(tileSet.getTile(BLANK_COIN));
            Hud.addScore(100);
        }
    }
}
