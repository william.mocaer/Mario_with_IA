package com.kibo.game.Sprites.TilesObjects;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.Body;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Scenes.Hud;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Mario;

/**
 * Created by willi on 19/05/2017.
 */

public class Brick extends InteractiveTileObject {
    public Brick(PlayScreen screen, MapObject object) {
        super(screen, object);
        fixture.setUserData(this);
        setCategoryFilter(MarioBrossTuto.BRICK_BIT);
    }

    public Brick(PlayScreen pl, Body body1) {
        super(pl,body1);
//        fixture.setUserData(this);
        setCategoryFilter(MarioBrossTuto.BRICK_BIT);
    }

    @Override
    public void onHeadHit(Mario mario) {
        if(mario.isBig())
        {
            setCategoryFilter(MarioBrossTuto.DESTROYED_BIT);
            getCell().setTile(null);
            Hud.addScore(200);
            MarioBrossTuto.manager.get("audio/sounds/breakblock.wav", Sound.class).play();
        }else
            MarioBrossTuto.manager.get("audio/sounds/bump.wav", Sound.class).play();

    }
}
