package com.kibo.game.Sprites.Enemies;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Mario;
import com.kibo.game.Tools.B2WorldCreator;

/**
 * Created by willi on 20/05/2017.
 */

public class Goomba extends com.kibo.game.Sprites.Enemies.Enemy {
    private float stateTime;
    private Animation<TextureRegion> walkAnimation;
    private Array<TextureRegion> frames;
    private boolean setToDestroy;
    private boolean destroyed;
    private B2WorldCreator creator;

    public Goomba(PlayScreen screen, float x, float y) {
        super(screen, x, y);
        frames=new Array<TextureRegion>();
        for(int i = 0 ; i<2 ;i++)
        {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("goomba"), i*16,0,16,16));
        }
        walkAnimation=new Animation<TextureRegion>(0.4f,frames);
        stateTime=0;
        setBounds(getX(),getY(),16/MarioBrossTuto.PPM,16/MarioBrossTuto.PPM);
        setToDestroy=false;
        destroyed=false;
    }

    public Goomba(PlayScreen pl, Body body1) {
        super(pl,body1);
        frames=new Array<TextureRegion>();
        for(int i = 0 ; i<2 ;i++)
        {
            frames.add(new TextureRegion(screen.getAtlas().findRegion("goomba"), i*16,0,16,16));
        }
        walkAnimation=new Animation<TextureRegion>(0.4f,frames);
        stateTime=0;
        setBounds(getX(),getY(),16/MarioBrossTuto.PPM,16/MarioBrossTuto.PPM);
        setToDestroy=false;
        destroyed=false;
    }

    public void update(float dt)
    {
        stateTime+=dt;
        if(setToDestroy && !destroyed) {
            screen.getWorld().destroyBody(b2body);
            destroyed = true;
            setRegion(new TextureRegion(screen.getAtlas().findRegion("goomba"), 32, 0, 16, 16));
            stateTime=0;

        }else if(!destroyed)
        {
            b2body.setLinearVelocity(velocity);
            setPosition(b2body.getPosition().x - getWidth() / 2, b2body.getPosition().y - getHeight() /2);
            setRegion(walkAnimation.getKeyFrame(stateTime,true));
        }

    }

    @Override
    public void onEnemyHit(Enemy enemy)
    {
        if(enemy instanceof Turtle && ((Turtle)enemy).getCurrentState()== Turtle.State.MOVING_SHELL)
            setToDestroy=true;
        else
            reverseVelocity(true,false);
    }

    @Override
    protected void defineEnemy() {
        BodyDef bdef=new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body = screen.getWorld().createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(5/MarioBrossTuto.PPM);
        fdef.filter.categoryBits=MarioBrossTuto.ENEMY_BIT;
        fdef.filter.maskBits = MarioBrossTuto.GROUND_BIT |
                MarioBrossTuto.COIN_BIT |
                MarioBrossTuto.BRICK_BIT |
                MarioBrossTuto.ENEMY_BIT |
                MarioBrossTuto.OBJECT_BIT |
                MarioBrossTuto.MARIO_BIT;

        fdef.shape=shape;
        b2body.createFixture(fdef).setUserData(this );

        //create ht ehead

        PolygonShape head = new PolygonShape();
        Vector2[] vertice = new Vector2[4];
        vertice[0] = new Vector2(-8, 9).scl(1/MarioBrossTuto.PPM);
        vertice[1] = new Vector2(8,9).scl(1/MarioBrossTuto.PPM);
        vertice[2] = new Vector2(-7,5).scl(1/MarioBrossTuto.PPM);
        vertice[3] = new Vector2(7,5).scl(1/MarioBrossTuto.PPM);
         head.set(vertice);

        fdef.shape=head;
        fdef.restitution = 1.5f;
        fdef.filter.categoryBits = MarioBrossTuto.ENEMY_HEAD_BIT;

        b2body.createFixture(fdef).setUserData(this);
    }

    public void draw(Batch batch)
    {
        if(!destroyed ||stateTime<1)
        {
            super.draw(batch);
        }
    }


    @Override
    public void hitOnHead(Mario mario)
    {
        setToDestroy=true;
        MarioBrossTuto.manager.get("audio/sounds/stomp.wav", Sound.class).play();

    }

    public void setCreator(B2WorldCreator creator) {
        this.creator = creator;
    }
}
