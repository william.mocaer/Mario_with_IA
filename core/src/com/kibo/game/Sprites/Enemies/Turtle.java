package com.kibo.game.Sprites.Enemies;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Mario;
import com.kibo.game.Tools.B2WorldCreator;

/**
 * Created by willi on 21/05/2017.
 */

public class Turtle extends Enemy
{
    public static final int KICK_LEFT_SPEED = -2;
    public static final int KICK_RIGHT_SPEED = -KICK_LEFT_SPEED;



    private B2WorldCreator creator;

    public enum State {WALKING, STANDING_SHELL,MOVING_SHELL,DEAD};
    public State currentState;
    public State previousState;


    private float stateTime;
    private Animation<TextureRegion> walkAnimation;
    private Animation<TextureRegion> shellAnimation;

    private Array<TextureRegion> frames;

    private float deadRotationDegrees;


    private boolean setToDestroy;
    private boolean destroyed;



    public Turtle(PlayScreen screen, float x, float y) {
        super(screen, x, y);
        frames= new Array<TextureRegion>();
        frames.add(new TextureRegion(screen.getAtlas().findRegion("turtle"),0,0,16,24));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("turtle"),16,0,16,24));
        walkAnimation = new Animation<TextureRegion>(0.2f,frames);
        currentState = previousState = State.WALKING;

        frames.clear();

        frames.add(new TextureRegion(screen.getAtlas().findRegion("turtle"),64,0,16,24));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("turtle"),64+16,0,16,24));
        shellAnimation=new Animation<TextureRegion>(0.2f,frames);
        deadRotationDegrees=0;
        setBounds(getX(),getY(),16 / MarioBrossTuto.PPM, 24 / MarioBrossTuto.PPM);
    }

    public Turtle(PlayScreen screen,Body b) {
        super(screen,b);
        frames= new Array<TextureRegion>();
        frames.add(new TextureRegion(screen.getAtlas().findRegion("turtle"),0,0,16,24));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("turtle"),16,0,16,24));
        walkAnimation = new Animation<TextureRegion>(0.2f,frames);
        currentState = previousState = State.WALKING;

        frames.clear();

        frames.add(new TextureRegion(screen.getAtlas().findRegion("turtle"),64,0,16,24));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("turtle"),64+16,0,16,24));
        shellAnimation=new Animation<TextureRegion>(0.2f,frames);
        deadRotationDegrees=0;
        setBounds(getX(),getY(),16 / MarioBrossTuto.PPM, 24 / MarioBrossTuto.PPM);
    }

    @Override
    protected void defineEnemy() {
        BodyDef bdef=new BodyDef();
        bdef.position.set(getX(),getY());
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body = screen.getWorld().createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6/MarioBrossTuto.PPM);
        fdef.filter.categoryBits=MarioBrossTuto.ENEMY_BIT;
        fdef.filter.maskBits = MarioBrossTuto.GROUND_BIT |
                MarioBrossTuto.COIN_BIT |
                MarioBrossTuto.BRICK_BIT |
                MarioBrossTuto.ENEMY_BIT |
                MarioBrossTuto.OBJECT_BIT |
                MarioBrossTuto.MARIO_BIT;

        fdef.shape=shape;
        b2body.createFixture(fdef).setUserData(this );

        //create ht ehead

        PolygonShape head = new PolygonShape();
        Vector2[] vertice = new Vector2[4];
        vertice[0] = new Vector2(-6,9).scl(1/MarioBrossTuto.PPM);
        vertice[1] = new Vector2(6,9).scl(1/MarioBrossTuto.PPM);
        vertice[2] = new Vector2(-4,3).scl(1/MarioBrossTuto.PPM);
        vertice[3] = new Vector2(4,3).scl(1/MarioBrossTuto.PPM);
        head.set(vertice);

        fdef.shape=head;
        fdef.restitution = 0.7f;//bounce on it next to 0 = less bumping
        fdef.filter.categoryBits = MarioBrossTuto.ENEMY_HEAD_BIT;

        b2body.createFixture(fdef).setUserData(this);
    }
    public void setCreator(B2WorldCreator creator) {
        this.creator = creator;
    }
    @Override
    public void hitOnHead(Mario mario) {
        if(currentState!= State.STANDING_SHELL)
        {
            currentState=State.STANDING_SHELL;
            velocity.x=0;
        }else
        {
            kick(mario.getX() <=this.getX() ? KICK_RIGHT_SPEED: KICK_LEFT_SPEED);
        }
    }

    public void kick(int speed)
    {
        velocity.x = speed;
        currentState=State.MOVING_SHELL;
    }

    public State getCurrentState()
    {
        return currentState;
    }

    public TextureRegion getFrame(float dt)
    {
        TextureRegion region;
        switch (currentState)
        {
            case STANDING_SHELL:
                region = shellAnimation.getKeyFrame(0);
                break;
            case MOVING_SHELL:
                region = shellAnimation.getKeyFrame(stateTime,true);
                break;
            case WALKING:
            default:
                region = walkAnimation.getKeyFrame(stateTime,true);
                break;
        }

        if(velocity.x > 0 && !region.isFlipX())
            region.flip(true,false);
        if(velocity.x <  0 && region.isFlipX())
            region.flip(true,false);
        stateTime = currentState==previousState? stateTime+dt:0;
        previousState=currentState;
        return region;
    }

    @Override
    public void update(float dt) {
        setRegion(getFrame(dt));
        if(currentState== State.STANDING_SHELL && stateTime > 5 )
        {
            currentState=State.WALKING;
            velocity.x=1;
        }

        setPosition(b2body.getPosition().x - getWidth()/2,b2body.getPosition().y - 8/MarioBrossTuto.PPM);

       if(currentState==State.DEAD)
       {
           deadRotationDegrees+=3;
            rotate(deadRotationDegrees);
           if(stateTime>5 && !destroyed)
           {
               screen.getWorld().destroyBody(b2body);
               destroyed=true;
               creator.destroyTurtle(this);
           }
       }else
            b2body.setLinearVelocity(velocity);

    }

    @Override
    public void onEnemyHit(Enemy enemy) {
        if(enemy instanceof Turtle)
        {
            Turtle turtle = (Turtle)enemy;
            if(turtle.currentState == State.MOVING_SHELL && currentState!=State.MOVING_SHELL)
                killed();
            else if(currentState == State.MOVING_SHELL && turtle.currentState==State.WALKING)
                return;
            else
                reverseVelocity(true,false);

        }else if(currentState != State.MOVING_SHELL)
        {
            reverseVelocity(true,false);
        }
    }

    public void killed()
    {
        currentState=State.DEAD;
        Filter filter= new Filter();
        filter.maskBits=0;
        for(Fixture fixture: b2body.getFixtureList())
            fixture.setFilterData(filter);
        b2body.applyLinearImpulse(new Vector2(0,5f),b2body.getWorldCenter(),true);
    }

    public void draw(Batch batch)
    {
        if(!destroyed)
            super.draw(batch);
    }
}
