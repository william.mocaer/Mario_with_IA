package com.kibo.game.Sprites.Enemies;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Mario;

/**
 * Created by willi on 20/05/2017.
 */

public abstract class Enemy extends Sprite
{
    protected PlayScreen screen;
    public Body b2body;
    public Vector2 velocity;

    public Enemy(PlayScreen screen, float x, float y)
    {
        this.screen=screen;
        setPosition(x,y);
        defineEnemy();
        velocity=new Vector2(-1,-2);
        b2body.setActive(false);
    }


    public Enemy(PlayScreen screen, Body body)
    {
        this.screen=screen;
       this.b2body=body;
        velocity=new Vector2(-1,-2);
    }


    protected abstract void defineEnemy();

    public abstract void hitOnHead(Mario mario);

    public void reverseVelocity(boolean x,boolean y)
    {
        if(x)
            velocity.x=-velocity.x;
        if(y)
            velocity.y=-velocity.y;
    }

    public abstract void update(float dt);

    public abstract void onEnemyHit(Enemy enemy);
}
