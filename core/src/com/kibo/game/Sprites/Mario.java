package com.kibo.game.Sprites;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.utils.Array;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Screens.PlayScreen;
import com.kibo.game.Sprites.Enemies.Enemy;
import com.kibo.game.Sprites.Enemies.Turtle;

/**
 * Created by willi on 19/05/2017.
 */

public class Mario extends Sprite {


    private  PlayScreen screen;
    private float timerCheckState;

    public Vector2 getPosition() {
        return b2body.getPosition();
    }




    public enum State {FALLING, JUMPING, STANDING, RUNNING, GROWING, DEAD};
    public State currentState;
    public State previousState;

    public Body b2body;

    private TextureRegion marioStand;
    private Animation<TextureRegion> marioRun;
    private TextureRegion marioJump;
    private TextureRegion marioDead;
    private TextureRegion bigMarioStand;
    private TextureRegion bigMarioJump;
    private Animation<TextureRegion> bigMarioRun;
    private Animation<TextureRegion> growMario;

    private float stateTimer;
    private boolean marioIsBig;
    private boolean runGrowAnimation;
    private boolean timeToDefineBigMario;
    private boolean timeToRedefineBigMario;
    private boolean runningRight;
    private boolean marioIsDead;
    public Mario(PlayScreen screen)
    {
        this.screen =screen;

        currentState = State.STANDING;
        previousState= State.STANDING;
        stateTimer = 0;
        runningRight=true;
        timerCheckState=0;
        int height = screen.getAtlas().findRegion("little_mario").getRegionHeight();
        int width = screen.getAtlas().findRegion("little_mario").getRegionWidth()/14;

        Array<TextureRegion> frames = new Array<TextureRegion>();
        for(int i = 1 ; i<4;i++)
            frames.add(new TextureRegion(screen.getAtlas().findRegion("little_mario"), i * width,0,width,height));
        marioRun=new Animation<TextureRegion>(0.1f,frames);
        frames.clear();

        for(int i = 1 ; i<4;i++)
            frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),  i * 16,0,16,32));
        bigMarioRun=new Animation<TextureRegion>(0.1f,frames);
        frames.clear();

        //growing mario
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),240,0,16,32));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),0,0,16,32));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),240,0,16,32));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),0,0,16,32));

        growMario = new Animation<TextureRegion>(0.2f,frames);

        //jump anim
        marioJump = new TextureRegion(screen.getAtlas().findRegion("little_mario"),80,0,16,16);
        bigMarioJump = new TextureRegion(screen.getAtlas().findRegion("big_mario"),80,0,16,32);


        //create TEXTURE region for mario standing
        this.marioStand =new TextureRegion(screen.getAtlas().findRegion("little_mario"),0,0,width,height);
        bigMarioStand = new TextureRegion(screen.getAtlas().findRegion("big_mario"),0,0,16,32);

        //create Texture for mariodead
        marioDead = new TextureRegion(screen.getAtlas().findRegion("little_mario"),96,0,16,16);


        defineMario();
         setBounds(0,0,16 /MarioBrossTuto.PPM,16/MarioBrossTuto.PPM);
        setRegion(marioStand);
    }

    public Mario(PlayScreen screen, Body body)
    {
        this.screen =screen;
        this.b2body=body;
        currentState = State.STANDING;
        previousState= State.STANDING;
        stateTimer = 0;
        runningRight=true;

        int height = screen.getAtlas().findRegion("little_mario").getRegionHeight();
        int width = screen.getAtlas().findRegion("little_mario").getRegionWidth()/14;

        Array<TextureRegion> frames = new Array<TextureRegion>();
        for(int i = 1 ; i<4;i++)
            frames.add(new TextureRegion(screen.getAtlas().findRegion("little_mario"), i * width,0,width,height));
        marioRun=new Animation<TextureRegion>(0.1f,frames);
        frames.clear();

        for(int i = 1 ; i<4;i++)
            frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),  i * 16,0,16,32));
        bigMarioRun=new Animation<TextureRegion>(0.1f,frames);
        frames.clear();

        //growing mario
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),240,0,16,32));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),0,0,16,32));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),240,0,16,32));
        frames.add(new TextureRegion(screen.getAtlas().findRegion("big_mario"),0,0,16,32));

        growMario = new Animation<TextureRegion>(0.2f,frames);

        //jump anim
        marioJump = new TextureRegion(screen.getAtlas().findRegion("little_mario"),80,0,16,16);
        bigMarioJump = new TextureRegion(screen.getAtlas().findRegion("big_mario"),80,0,16,32);


        //create TEXTURE region for mario standing
        this.marioStand =new TextureRegion(screen.getAtlas().findRegion("little_mario"),0,0,width,height);
        bigMarioStand = new TextureRegion(screen.getAtlas().findRegion("big_mario"),0,0,16,32);

        //create Texture for mariodead
        marioDead = new TextureRegion(screen.getAtlas().findRegion("little_mario"),96,0,16,16);

        setBounds(0,0,16 /MarioBrossTuto.PPM,16/MarioBrossTuto.PPM);
        setRegion(marioStand);
    }

    public void update(float dt)
    {
        if(b2body.getPosition().y<-0.2f && !marioIsDead)
        {
            marioIsDead=true;
            MarioBrossTuto.manager.get("audio/sounds/mariodie.wav", Sound.class).play();

        }
       // System.out.println(b2body.getPosition().x);
        if(marioIsBig)
        {
            setPosition(b2body.getPosition().x-getWidth()/2,b2body.getPosition().y-getHeight()/2 - 6/MarioBrossTuto.PPM);
        }
        else
        {
            setPosition(b2body.getPosition().x-getWidth()/2,b2body.getPosition().y-getHeight()/2);
         /*  Vector2 fututrePos = Predictor.getFuturePosition(b2body,dt,screen.getWorld());
            setPosition(fututrePos.x-getWidth()/2,fututrePos.y);*/
          //  System.out.println("DIFF "+ (b2body.getPosition().x-fututrePos.x) +"      Y :   "+ (b2body.getPosition().y-fututrePos.y) );
        }
        setRegion(getFrame(dt));
        if(timeToDefineBigMario)
            defineBigMario();
        if(timeToRedefineBigMario)
            redefineBigMario();
    }

    private void redefineBigMario()
    {
        Vector2 position = b2body.getPosition();
        screen.getWorld().destroyBody(b2body);

        BodyDef bdef=new BodyDef();
        bdef.position.set(position);
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body =  screen.getWorld().createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6/MarioBrossTuto.PPM);
        fdef.filter.categoryBits=MarioBrossTuto.MARIO_BIT;
        fdef.filter.maskBits =
                MarioBrossTuto.GROUND_BIT |
                        MarioBrossTuto.COIN_BIT |
                        MarioBrossTuto.BRICK_BIT |
                        MarioBrossTuto.OBJECT_BIT |
                        MarioBrossTuto.ENEMY_BIT |
                        MarioBrossTuto.ENEMY_HEAD_BIT |
                        MarioBrossTuto.ITEM_BIT ;
        fdef.shape=shape;
        b2body.createFixture(fdef).setUserData(this);

        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-2 / MarioBrossTuto.PPM, 6 / MarioBrossTuto.PPM), new Vector2(2 / MarioBrossTuto.PPM, 6 / MarioBrossTuto.PPM));
        fdef.filter.categoryBits=MarioBrossTuto.MARIO_HEAD_BIT;

        fdef.shape=head;
        fdef.isSensor=true;

        b2body.createFixture(fdef).setUserData(this);
        timeToRedefineBigMario=false;
    }

    private void defineBigMario()
    {
        Vector2 currentPosition = b2body.getPosition();
        screen.getWorld().destroyBody(b2body);

        BodyDef bdef=new BodyDef();
        bdef.position.set(currentPosition.add(0,10/MarioBrossTuto.PPM));
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body =  screen.getWorld().createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6/MarioBrossTuto.PPM);
        fdef.filter.categoryBits=MarioBrossTuto.MARIO_BIT;
        fdef.filter.maskBits =
                MarioBrossTuto.GROUND_BIT |
                        MarioBrossTuto.COIN_BIT |
                        MarioBrossTuto.BRICK_BIT |
                        MarioBrossTuto.OBJECT_BIT |
                        MarioBrossTuto.ENEMY_BIT |
                        MarioBrossTuto.ENEMY_HEAD_BIT |
                        MarioBrossTuto.ITEM_BIT ;
        fdef.shape=shape;
        b2body.createFixture(fdef).setUserData(this);
        shape.setPosition(new Vector2(0,-14/MarioBrossTuto.PPM));
        b2body.createFixture(fdef).setUserData(this);

        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-2 / MarioBrossTuto.PPM, 6 / MarioBrossTuto.PPM), new Vector2(2 / MarioBrossTuto.PPM, 6 / MarioBrossTuto.PPM));
        fdef.filter.categoryBits=MarioBrossTuto.MARIO_HEAD_BIT;

        fdef.shape=head;
        fdef.isSensor=true;

        b2body.createFixture(fdef).setUserData(this);
        timeToDefineBigMario=false;

    }

    private TextureRegion getFrame(float dt) {
        previousState = currentState;
        currentState=getState();
        TextureRegion region;
        switch (currentState)
        {
            case DEAD:
                region=marioDead;
                break;
            case GROWING:
                region=growMario.getKeyFrame(stateTimer);
                if (growMario.isAnimationFinished(stateTimer))
                    runGrowAnimation = false;
                break;
            case JUMPING:
                region=marioIsBig?bigMarioJump:marioJump;
                break;
            case RUNNING:
                region=marioIsBig?bigMarioRun.getKeyFrame(stateTimer,true):marioRun.getKeyFrame(stateTimer,true);
                break;
            case FALLING:
            case STANDING:
            default:
                region = marioIsBig?bigMarioStand:marioStand;
                break;
        }

        //not optimised at all
        if ((b2body.getLinearVelocity().x<0 || !runningRight) && !region.isFlipX())
        {
            region.flip(true,false);
            runningRight=false;
        }else if((b2body.getLinearVelocity().x>0 || runningRight) && region.isFlipX())
        {
            region.flip(true,false);
            runningRight=true;
        }
        stateTimer = currentState==previousState? stateTimer+dt:0;

        return region;
    }

    public State getState() {

        if(marioIsDead)
            return State.DEAD;
        if(runGrowAnimation)
            return State.GROWING;
        if(b2body.getLinearVelocity().y>0 || (b2body.getLinearVelocity().y<0 && (previousState==State.JUMPING || previousState==State.FALLING) ))
            return State.JUMPING;
        if(b2body.getLinearVelocity().y<0)
            return State.FALLING;
        if (b2body.getLinearVelocity().x!=0)
            return State.RUNNING;
        return State.STANDING;
    }

    public void grow()
    {
        runGrowAnimation =true;
        marioIsBig = true;
        timeToDefineBigMario = true;
        setBounds(getX(),getY(),getWidth(),getHeight()*2);
        MarioBrossTuto.manager.get("audio/sounds/powerup.wav", Sound.class).play();

    }

    private void defineMario() {
        BodyDef bdef=new BodyDef();
        bdef.position.set(32/ MarioBrossTuto.PPM,32/ MarioBrossTuto.PPM);
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body =  screen.getWorld().createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(6/MarioBrossTuto.PPM);
        fdef.filter.categoryBits=MarioBrossTuto.MARIO_BIT;
        fdef.filter.maskBits =
                MarioBrossTuto.GROUND_BIT |
                        MarioBrossTuto.COIN_BIT |
                        MarioBrossTuto.BRICK_BIT |
                        MarioBrossTuto.OBJECT_BIT |
                        MarioBrossTuto.ENEMY_BIT |
                        MarioBrossTuto.ENEMY_HEAD_BIT |
                        MarioBrossTuto.ITEM_BIT ;
        fdef.shape=shape;
        b2body.createFixture(fdef).setUserData(this);

        EdgeShape head = new EdgeShape();
        head.set(new Vector2(-2 / MarioBrossTuto.PPM, 8 / MarioBrossTuto.PPM), new Vector2(2 / MarioBrossTuto.PPM, 6 / MarioBrossTuto.PPM));
        fdef.filter.categoryBits=MarioBrossTuto.MARIO_HEAD_BIT;

        fdef.shape=head;
        fdef.isSensor=true;

        b2body.createFixture(fdef).setUserData(this);
    }

    public void hit(Enemy enemy)
    {
        if(enemy instanceof Turtle && ((Turtle)enemy).getCurrentState() == Turtle.State.STANDING_SHELL)
            ((Turtle)enemy).kick(this.getX()<=enemy.getX() ? Turtle.KICK_RIGHT_SPEED: Turtle.KICK_LEFT_SPEED);
        else
        {
            if(marioIsBig)
            {
                marioIsBig=false;
                timeToRedefineBigMario=true;
                setBounds(getX(),getY(),getWidth(),getHeight()/2);
                MarioBrossTuto.manager.get("audio/sounds/powerdown.wav", Sound.class).play();
            }else
            {
                if(MarioBrossTuto.manager.get("audio/music/mario_music.ogg",Music.class).isPlaying())
                    MarioBrossTuto.manager.get("audio/music/mario_music.ogg",Music.class).stop();
                MarioBrossTuto.manager.get("audio/sounds/mariodie.wav", Sound.class).play();
                marioIsDead=true;
                Filter filter = new Filter();
                filter.maskBits=0;
                for(Fixture fixture : b2body.getFixtureList())
                {
                    fixture.setFilterData(filter);
                }
                b2body.applyLinearImpulse(new Vector2(0,4f),b2body.getWorldCenter(),true);
            }
        }
    }

    public void setAlive()
    {
        marioIsDead=false;
    }


    public boolean isBig() {
        return marioIsBig;
    }
    public boolean isDead() {
        return marioIsDead;
    }

    public float getStateTimer()
    {
     return stateTimer;
    }
    public PlayScreen getScreen() {
        return screen;
    }

}
