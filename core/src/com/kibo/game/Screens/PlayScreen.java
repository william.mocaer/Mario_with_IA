package com.kibo.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Scenes.Hud;
import com.kibo.game.Sprites.Enemies.Enemy;
import com.kibo.game.Sprites.Enemies.Goomba;
import com.kibo.game.Sprites.Items.Item;
import com.kibo.game.Sprites.Items.ItemDef;
import com.kibo.game.Sprites.Items.Mushroom;
import com.kibo.game.Sprites.Mario;
import com.kibo.game.Tools.B2WorldCreator;
import com.kibo.game.Tools.BodiesWrapped;
import com.kibo.game.Tools.Goal;
import com.kibo.game.Tools.PlayScreenWrapped;
import com.kibo.game.Tools.WorldContactListener;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by willi on 18/05/2017.
 */

public class PlayScreen implements Screen
{


    private Mario player; // Mario class object
    private Hud hud;
    private World world;
    private B2WorldCreator creator;
    private OrthographicCamera gameCam;
    private WorldContactListener wcl;
    private Viewport gamePort;

    private MarioBrossTuto game;
    private OrthogonalTiledMapRenderer renderer;

    private TiledMap map;

    //box2d viariables
    private Box2DDebugRenderer b2dr;

    private TextureAtlas atlas;

    private Music music;

    private List<Item> items;
    private LinkedBlockingQueue<ItemDef> itemsToSpawn;

    private float accumulator;
    private List<String> actions;
    public Iterator<String> itAction;

    public PlayScreen(MarioBrossTuto game)
    {
        accumulator=0;
        atlas=new TextureAtlas(Gdx.files.internal("Mario_and_Enemies.pack"));

        this.game=game;

        gameCam=new OrthographicCamera();
        gamePort=new FitViewport(MarioBrossTuto.V_WIDTH  / MarioBrossTuto.PPM*0.5f , MarioBrossTuto.V_HEIGHT / MarioBrossTuto.PPM*0.5f,gameCam);
        hud=new Hud(game.batch);

        TmxMapLoader maploader =new TmxMapLoader();
        map=maploader.load("maps/"+MarioBrossTuto.map);
        renderer=new OrthogonalTiledMapRenderer(map, 1/ MarioBrossTuto.PPM);
        gameCam.position.set(gamePort.getWorldWidth()/2,gamePort.getWorldHeight() /2,0);
        world=new World(new Vector2(0,-10),true);
        b2dr=new Box2DDebugRenderer();
        creator= new B2WorldCreator(this);
        player=new Mario(this);
         wcl = new WorldContactListener();
        world.setContactListener(wcl);

        music = MarioBrossTuto.manager.get("audio/music/mario_music.ogg",Music.class);
        music.setLooping(true);
        //music.play();

        items=new LinkedList<Item>();
        itemsToSpawn=new LinkedBlockingQueue<ItemDef>();
    }

    public PlayScreen()
    {
        gameCam=new OrthographicCamera();
        gamePort=new FitViewport(MarioBrossTuto.V_WIDTH  / MarioBrossTuto.PPM , MarioBrossTuto.V_HEIGHT / MarioBrossTuto.PPM ,gameCam);
        hud=new Hud(MarioBrossTuto.batch);
        b2dr=new Box2DDebugRenderer();
        music = MarioBrossTuto.manager.get("audio/music/mario_music.ogg",Music.class);

        music.setLooping(true);
       // music.play();

        renderer=new OrthogonalTiledMapRenderer(map, 1/ MarioBrossTuto.PPM);
        gameCam.position.set(gamePort.getWorldWidth()/2,gamePort.getWorldHeight() /2,0);

    }

    public PlayScreen(MarioBrossTuto marioBrossTuto, List<String> solution) {
        this(marioBrossTuto);
    }


    public void hydrate(BodiesWrapped bodiesWrappedAndWorld) {
        this.world=bodiesWrappedAndWorld.world;
        this.atlas=bodiesWrappedAndWorld.atlas;
        this.game=bodiesWrappedAndWorld.game;
        this.player=bodiesWrappedAndWorld.mario;
        this.map=bodiesWrappedAndWorld.map;
        this.creator=bodiesWrappedAndWorld.creator;
        this.items = bodiesWrappedAndWorld.items;
        this.itemsToSpawn=bodiesWrappedAndWorld.itemsToSpawn;
        this.wcl=bodiesWrappedAndWorld.worldContactListener;

        //System.out.println("HYDRATATION");
    }

    public void spawnItem(ItemDef idef)
    {
        itemsToSpawn.add(idef);
    }


    public void handleSpawningItems()
    {
        if(!itemsToSpawn.isEmpty())
        {
            ItemDef idef= itemsToSpawn.poll();
            if(idef.type== Mushroom.class)
            {
                items.add(new Mushroom(this,idef.position.x,idef.position.y));
            }
        }
    }


    public TextureAtlas getAtlas()
    {
        return atlas;
    }

    @Override
    public void show() {

    }
    public boolean isMarioOnGround()
    {
        for (Rectangle rect : creator.grounds)
        {
            if(player.b2body.getPosition().x >rect.getX()/MarioBrossTuto.PPM && player.b2body.getPosition().x+player.getWidth()*2<rect.getX()/MarioBrossTuto.PPM+rect.getWidth()/MarioBrossTuto.PPM)
                return true;
        }
        return false;
    }

    public void handleInput(float dt,String actionToDo)
    {

      /*  if(Gdx.input.isTouched())
            gameCam.position.x +=100*dt;*/

        //System.out.println("IN MY WORLD, MY VELO is "+player.b2body.getLinearVelocity());
        if(!player.isDead())
        {
            Vector2 vel = player.b2body.getLinearVelocity();
          /*  if(  actionToDo.equals(PlayScreenWrapped.ACTIONS[1])  && (player.currentState== Mario.State.JUMPING ||  player.currentState== Mario.State.FALLING || vel.y!=0 ))
            {
                System.out.println("CANNOT JUMP  state="+player.currentState+ " old :"+player.previousState+" velo "+vel.y );
                System.out.println("AT "+player.b2body.getPosition().x+" ;; "+player.b2body.getPosition().y +" ");

            }*/

            if( (Gdx.input.justTouched() || Gdx.input.isKeyPressed(Input.Keys.UP) || actionToDo.equals(PlayScreenWrapped.ACTIONS[1]) )  && player.currentState!= Mario.State.JUMPING &&  player.currentState!= Mario.State.FALLING && vel.y==0)
            {
             /*   System.out.println("JUMPING");
                System.out.println("state "+player.currentState+" old;"+player.previousState);
                System.out.println("vel "+vel);
                System.out.println("JUMPING AT "+player.b2body.getPosition().x+" ;; "+player.b2body.getPosition().y +" ");*/
                player.b2body.applyLinearImpulse(new Vector2(0,4f),player.b2body.getWorldCenter(),true);
            }
            if((Gdx.input.getAccelerometerY()>2 || Gdx.input.isKeyPressed(Input.Keys.RIGHT)|| actionToDo.equals(PlayScreenWrapped.ACTIONS[0]) ) && player.b2body.getLinearVelocity().x<=2)
            {

                float desiredVel = 1.5f;
                //player.b2body.applyLinearImpulse(new Vector2(1f*SPEED,0), player.b2body.getWorldCenter(),true);
                float velChange = desiredVel - vel.x;
                float impulse = player.b2body.getMass() * velChange; //disregard time factor
                player.b2body.applyLinearImpulse( new Vector2(impulse,0),  player.b2body.getWorldCenter(),true );
            }
            if((Gdx.input.getAccelerometerY()<-2 || Gdx.input.isKeyPressed(Input.Keys.LEFT) || actionToDo.equals(PlayScreenWrapped.ACTIONS[2])) && player.b2body.getLinearVelocity().x>=-2)
            {

                float desiredVel = - 1.5f;
                //player.b2body.applyLinearImpulse(new Vector2(1f*SPEED,0), player.b2body.getWorldCenter(),true);
                float velChange = desiredVel - vel.x;
                float impulse = player.b2body.getMass() * velChange; //disregard time factor
                player.b2body.applyLinearImpulse( new Vector2(impulse,0),  player.b2body.getWorldCenter(),true );
            }
                                                                                           //0.1f
        }

        if(  Gdx.input.isKeyPressed(Input.Keys.P))
            System.out.println("POSITION : "+getPlayer().b2body.getPosition().x);
        if(  Gdx.input.isKeyPressed(Input.Keys.S))
            System.out.println("STATE : "+getPlayer().currentState);
        if(  Gdx.input.isKeyPressed(Input.Keys.D))
        {
            int diso = (int) Math.abs(getPlayer().b2body.getPosition().x * MarioBrossTuto.PPM - getGoal().getX() * MarioBrossTuto.PPM);
            System.out.println("Disorder : "+diso);
        }

        if(  Gdx.input.isKeyPressed(Input.Keys.T))
        {
            System.out.println("IS ON GROUND  : "+isMarioOnGround());
        }


    }


    private void doPhysicsStep(float deltaTime) {
        // fixed time step
        // max frame time to avoid spiral of death (on slow devices)
      /*  float frameTime = Math.min(deltaTime, 0.25f);*/
       /*  accumulator += deltaTime;

        while (accumulator >= PlayScreenWrapped.DELTATIME) {*/

            world.step(deltaTime,PlayScreenWrapped.VELOCITY_ITERATIONS, PlayScreenWrapped.POSITION_ITERATIONS);
         /*  accumulator -= PlayScreenWrapped.DELTATIME;

           }*/
    }

    public void update(float dt)
    {

        if(itAction!=null && itAction.hasNext())
            handleInput(dt,itAction.next());
        else
            handleInput(dt,"");

        doPhysicsStep(dt);


        handleSpawningItems();
        //world.step(dt,6,2);

        player.update(dt);

        for (Enemy enemy : creator.getEnemies())
        {
            enemy.update(dt);
            if(enemy.getX() < player.b2body.getPosition().x +224/ MarioBrossTuto.PPM)
                enemy.b2body.setActive(true);
        }

        for(Item item:items)
        {
            item.update(dt);
        }


        hud.update(dt);
        if(player.currentState!= Mario.State.DEAD)
            gameCam.position.x=player.b2body.getPosition().x;
        gameCam.update();
        renderer.setView(gameCam);

    }

    public void smallUpdate(float dt)
    {
      //  System.out.println(player.b2body.getLinearVelocity());

        player.update(dt);

        for(Item item:items)
        {
            item.update(dt);
        }
        for (Enemy enemy : creator.getEnemies())
        {
            enemy.update(dt);

            if(enemy.getX() < player.b2body.getPosition().x +224/ MarioBrossTuto.PPM)
                enemy.b2body.setActive(true);
        }
    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(0,0,0,1);
        Vector2 diff = new Vector2(0,0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        renderer.render();

        //renderer Box 2D
        b2dr.render(world,gameCam.combined);

       /* if(Gdx.app.getType() == Application.ApplicationType.Android)
        controlerPad.draw();*/
        game.batch.setProjectionMatrix(gameCam.combined);
        game.batch.begin();


        for (Enemy enemy : creator.getEnemies())
        {
            enemy.draw(game.batch);
            if (enemy.getX()<player.getX() +224 /MarioBrossTuto.PPM)
                enemy.b2body.setActive(true);
        }

        for (Item item:items)
        {
            item.draw(game.batch);
        }

        player.draw(game.batch);

        game.batch.end();

        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();

        if(gameOver())
        {
            game.setScreen(new GameOverScreen(game));
            dispose();
        }
    }


    public boolean gameOver()
    {
        return player.currentState == Mario.State.DEAD && player.getStateTimer()>3;

    }

    public boolean dieing() {
        return player.currentState == Mario.State.DEAD;
    }


        @Override
    public void resize(int width, int height) {
        gamePort.update(width,height);
    }

    public TiledMap getMap()
    {
        return map;
    }

    public World getWorld()
    {
        return world;
    }
    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        map.dispose();
        renderer.dispose();
        world.dispose();
        b2dr.dispose();
        hud.dispose();
    }

    public Mario getPlayer() {
        return player;
    }

    public MarioBrossTuto getGame() {
        return game;
    }

    public B2WorldCreator getCreator() {
        return creator;
    }

    public WorldContactListener getWorldContactListener() {
        return wcl;
    }

    public LinkedBlockingQueue<ItemDef> getItemsToSpawn() {
        return itemsToSpawn;
    }

    public void setAtlas(TextureAtlas atlas) {
        this.atlas = atlas;
    }

    public void setMap(TiledMap map) {
        this.map = map;
    }

    public void setActions(List<String> actions) {
        this.actions = actions;
        this.itAction= actions.iterator();
    }

    public boolean IsMarioAtGoal() {
        return player.getPosition().x>=creator.getGoal().getX();
    }

    public Goal getGoal() {
        return creator.getGoal();

    }

    public Goomba getGoomba() {
        return creator.getGoombas().get(0);
    }
}
