package com.kibo.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kibo.game.MarioBrossTuto;

/**
 * Created by willi on 21/05/2017.
 */

public class WelcomeScreen implements Screen
{

    private class LevelSelectListener extends ClickListener {
        private String level;
        private MarioBrossTuto game;

        LevelSelectListener(MarioBrossTuto game, String level)
        {
            this.game=game;
            this.level=level;
        }

        public void clicked(InputEvent event, float x, float y) {
            MarioBrossTuto.map=level;
            game.setScreen(new SelectModeScreen( game));
            dispose();
        }
    }
    private Table table;
    private Viewport viewport;
    private Stage stage;

    private Game game;
    private boolean exported;

    public WelcomeScreen(Game game)
    {
        this.game=game;
        viewport=new FitViewport(MarioBrossTuto.V_WIDTH, MarioBrossTuto.V_HEIGHT, new OrthographicCamera());
        stage=new Stage(viewport,((MarioBrossTuto)game).batch);
        MarioBrossTuto sp = ((MarioBrossTuto)game);
        Gdx.input.setInputProcessor(stage);

        Table table=new Table();
        table.setSize(MarioBrossTuto.V_WIDTH, MarioBrossTuto.V_HEIGHT);


        Label real =new Label("Selection niveau",sp.getSkin());
        table.add(real).padTop(20).colspan(3);
        table.row();

        FileHandle mapsFolder = Gdx.files.internal("maps");
        int i=0;
        for (FileHandle entry: mapsFolder.list()) {
            if(entry.extension().equals("tmx"))
            {
                i++;
                TextButton level=new TextButton(entry.name(),sp.getSkin());
                level.addListener(new LevelSelectListener((MarioBrossTuto)game,entry.name()));
                table.add(level).padTop(15);
                if(i%3==0)
                    table.row();
            }
        }



        stage.addActor(table);
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.1f,0.1f,0.1f, 1);
        Gdx.gl.glClear( 16384);

        // let the stage act and draw
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
