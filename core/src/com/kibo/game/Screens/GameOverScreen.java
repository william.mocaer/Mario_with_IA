package com.kibo.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kibo.game.MarioBrossTuto;

/**
 * Created by willi on 21/05/2017.
 */

public class GameOverScreen implements Screen
{
    private Table table;
    private Viewport viewport;
    private Stage stage;

    private Game game;
    private boolean exported;

    public GameOverScreen(final Game game)
    {
        System.out.println("end");
        this.game=game;
        viewport=new FitViewport(MarioBrossTuto.V_WIDTH,MarioBrossTuto.V_HEIGHT, new OrthographicCamera());
        stage=new Stage(viewport,MarioBrossTuto.batch);

        Gdx.input.setInputProcessor(stage);

         table = new Table();
        table.setSize(MarioBrossTuto.V_WIDTH, MarioBrossTuto.V_HEIGHT);

        Label gameOverLabel = new Label("GAME OVER",((MarioBrossTuto)game).getSkin());
        table.add(gameOverLabel).colspan(2);
        table.row();

        TextButton Menu=new TextButton("MENU",((MarioBrossTuto)game).getSkin());
        Menu.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                GameOverScreen.this.game.setScreen(new SelectModeScreen(GameOverScreen.this.game));
            }
        });
        TextButton Play=new TextButton("Play",((MarioBrossTuto)game).getSkin());
        Play.addListener(new ClickListener()
        {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ((MarioBrossTuto) GameOverScreen.this.game).sc = new PlayScreen((MarioBrossTuto) GameOverScreen.this.game);
                ((MarioBrossTuto) GameOverScreen.this.game).sc.setActions(((MarioBrossTuto)  GameOverScreen.this.game).solution);
                GameOverScreen.this.game.setScreen(((MarioBrossTuto) GameOverScreen.this.game).sc);
            }
        });
        table.add(Menu).padTop(20);
        table.add(Play).padTop(20);

        stage.addActor(table);
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        if(Gdx.input.isKeyPressed(Input.Keys.E) && !exported)
        {
            exported=true;
            ((MarioBrossTuto) game).exportSolution();

            Label gameOverLabel = new Label("Exported",((MarioBrossTuto)game).getSkin());
            table.row();
            table.add(gameOverLabel).colspan(2);
        }

        Gdx.gl.glClearColor(0.1f,0.1f,0.1f, 1);
        Gdx.gl.glClear( 16384);

        // let the stage act and draw
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
