package com.kibo.game.Screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.kibo.game.MarioBrossTuto;
import com.kibo.game.Tools.IA.Solver;
import com.kibo.game.Tools.IA.SolverIDASTART_Wiki;
import com.kibo.game.Tools.IA.SolverJump;
import com.kibo.game.Tools.IA.SolverMaxLenPQ;

import java.util.ArrayList;

/**
 * Created by willi on 21/05/2017.
 */

public class SelectModeScreen implements Screen
{
    enum Solvers {SolverBasic,SolverIDAStar,SolverJump,SolverMaxLenPQ,Free};

    private class CalculIAListener extends ClickListener {
        private  Solvers solver;
        private MarioBrossTuto game;

        CalculIAListener(MarioBrossTuto game,Solvers s)
        {
            this.game=game;
            this.solver = s;
        }

        public void clicked(InputEvent event, float x, float y) {
            //solution = SolverJump.solveMain(this);
            game.sc = new PlayScreen(game);

            switch (solver)
            {
                case SolverBasic:
                    game.solution = Solver.solveMain(game);
                    game.solution.remove(0);
                break;
                case SolverIDAStar:
                    game.solution = SolverIDASTART_Wiki.solveMain(game);
                    break;
                case SolverJump:
                    game.solution = SolverJump.solveMain(game);
                    break;
                case SolverMaxLenPQ:
                    game.solution = SolverMaxLenPQ.solveMain(game);
                    break;
                default:
                    game.solution=new ArrayList<String>();
            }

            game.sc.setActions(game.solution);
            game.setScreen( game.sc);
            dispose();
        }
    }

    private  class LoadIA extends ClickListener {
        private MarioBrossTuto game;
        LoadIA(MarioBrossTuto game)
        {
            this.game=game;
        }

        public void clicked(InputEvent event, float x, float y) {
            game.loadSolution();
            game.sc=new PlayScreen(game);
            game.sc.setActions(game.solution);
            game.setScreen( game.sc);
            dispose();
        }
    }
    private  class Back extends ClickListener {
        private MarioBrossTuto game;
        Back(MarioBrossTuto game)
        {
            this.game=game;
        }

        public void clicked(InputEvent event, float x, float y) {
            game.setScreen(new WelcomeScreen( game));
            dispose();
        }
    }

    private Table table;
    private Viewport viewport;
    private Stage stage;

    private Game game;
    private boolean exported;

    public SelectModeScreen(Game game)
    {
        this.game=game;
        viewport=new FitViewport(MarioBrossTuto.V_WIDTH, MarioBrossTuto.V_HEIGHT, new OrthographicCamera());
        stage=new Stage(viewport,((MarioBrossTuto)game).batch);
        MarioBrossTuto sp = ((MarioBrossTuto)game);
        Gdx.input.setInputProcessor(stage);

        Table table=new Table();
        table.setSize(MarioBrossTuto.V_WIDTH, MarioBrossTuto.V_HEIGHT);


        Label real =new Label("Niveau "+MarioBrossTuto.map,sp.getSkin());
        table.add(real).padTop(20).colspan(3);
        table.row();

        Label s =new Label("Selection Solver : ",sp.getSkin());
        table.add(s).padTop(20).colspan(3);
        table.row();

        FileHandle mapsFolder = Gdx.files.internal("solutions");
        boolean solExist=false;
        for(FileHandle h :mapsFolder.list())
        {
            if(h.name().equals(MarioBrossTuto.map+".sol"))
            {
                solExist=true;
                break;
            }
        }

        TextButton calculA=new TextButton("Solver A*",sp.getSkin());
        calculA.addListener(new CalculIAListener((MarioBrossTuto)game,Solvers.SolverBasic));
        table.add(calculA).padTop(10);

        TextButton calculIDA=new TextButton("Solver IDA*",sp.getSkin());
        calculIDA.addListener(new CalculIAListener((MarioBrossTuto)game,Solvers.SolverIDAStar));
        table.add(calculIDA).padTop(10);

        table.row();

        TextButton calculJump=new TextButton("Solver Jump",sp.getSkin());
        calculJump.addListener(new CalculIAListener((MarioBrossTuto)game,Solvers.SolverJump));
        table.add(calculJump).padTop(10);

        TextButton calculLenPQ=new TextButton("Solver Len PQ",sp.getSkin());
        calculLenPQ.addListener(new CalculIAListener((MarioBrossTuto)game,Solvers.SolverMaxLenPQ));
        table.add(calculLenPQ).padTop(10);

        table.row();

        TextButton load=new TextButton("Load calculated IA",sp.getSkin());
        load.setTouchable(solExist?Touchable.enabled:Touchable.disabled);
        load.setColor(solExist?Color.GREEN:Color.DARK_GRAY);
        load.addListener(new LoadIA((MarioBrossTuto)game));
        table.add(load).padTop(23).colspan(2);

        table.row();

        TextButton free=new TextButton("Free",sp.getSkin());
        free.setColor(Color.GREEN);
        free.addListener(new CalculIAListener((MarioBrossTuto)game,Solvers.Free));
        table.add(free).padTop(20).colspan(2);

        table.row();

        TextButton back=new TextButton("Back",sp.getSkin());
        back.addListener(new Back((MarioBrossTuto)game));
        table.add(back).padTop(40).colspan(2);

        stage.addActor(table);
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.1f,0.1f,0.1f, 1);
        Gdx.gl.glClear( 16384);

        // let the stage act and draw
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
